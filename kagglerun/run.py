import os
import sys

import sqlite3
# os.system('pip install --upgrade transformers') #4.1.1
os.system('pip install --upgrade datasets')
os.system('pip install comet_ml')

os.environ['COMET_API_KEY'] = 'ApPW0jmKXzW83jDVDPpDdkLh4'
os.environ['COMET_PROJECT_NAME'] = 'gpt2finnish'
os.environ['COMET_MODE'] = 'ONLINE'
os.environ['COMET_WORKSPACE'] = 'remotejob'

WANDB_PROJECT ='gpt2finnish'
WANDB_LOG_MODEL=True
WANDB_WATCH='all'


import comet_ml
from comet_ml import Experiment

import re
import json
from sklearn.model_selection import train_test_split
# from shutil import copyfile
import pandas as pd
from datasets import load_dataset,Dataset

import wandb


# pretrainedmodel ='distilgpt2'
# pretrainedmodel ='gpt2'
pretrainedmodel ='gpt2-medium'
# pretrainedmodel ='../input/gpt2finnishdata/gpt2-last'

from transformers import AutoTokenizer

# tokenizer = AutoTokenizer.from_pretrained(pretrainedmodel)
#tokenizer = AutoTokenizer.from_pretrained('gpt2')
tokenizer = AutoTokenizer.from_pretrained('gpt2-medium')

train_path = '../input/gpt2finnishdata/filteredtw_train.txt'
val_path = '../input/gpt2finnishdata/filteredtw_val.txt'


from transformers import TextDataset,DataCollatorForLanguageModeling

os.system('wandb login c6fd4e313051ec1cd535f712eb55345d5182459f')

# run = wandb.init(project="gpt2finnish")

epochs = 1
eval_steps = 400
warmup_steps=500
batch_size=32
runnum = 0
acc= 'alesandermazurov'
best = 0

run = wandb.init(project="gpt2finnish", config={"epochs": epochs, "trainfile": train_path,"eval_steps": eval_steps, "warmup_steps": warmup_steps, "best": best, 'batch_size': batch_size,'runnum':runnum,'acc':acc})

def load_datasetlocal(train_path,val_patch,tokenizer):  #last 200
    train_dataset = TextDataset(
          tokenizer=tokenizer,
          file_path=train_path,
          cache_dir='./',
          block_size=64)
     
    val_dataset = TextDataset(
          tokenizer=tokenizer,
          file_path=val_patch,
          cache_dir='./',
          block_size=64)   
    
    data_collator = DataCollatorForLanguageModeling(
        tokenizer=tokenizer, mlm=False,
    )
    return train_dataset,val_dataset,data_collator

train_dataset,val_dataset,data_collator = load_datasetlocal(train_path,val_path,tokenizer)

from transformers import Trainer, TrainingArguments,AutoModelWithLMHead

model = AutoModelWithLMHead.from_pretrained(pretrainedmodel)

def compute_metrics(pred):
    wandb.log({'pred',pred})
    experiment = comet_ml.config.get_global_experiment()

training_args = TrainingArguments(
    output_dir="./gpt2-last", #The output directory
    overwrite_output_dir=True, #overwrite the content of the output directory
    num_train_epochs=epochs, # number of training epochs #4
    # per_device_train_batch_size=64, # batch size for training #32
    # per_device_eval_batch_size=64,  # batch size for evaluation
    per_device_train_batch_size=batch_size, # batch size for training #32
    per_device_eval_batch_size=batch_size,  # batch size for evaluation   
    eval_steps = eval_steps, # Number of update steps between two evaluations.
    save_steps=800000000, # after # steps model is saved #800 80000000000 Neave save
    warmup_steps=warmup_steps,# number of warmup steps for learning rate scheduler
    save_total_limit=1,
    fp16=True,
    run_name="gpt2finnish",
    )


trainer = Trainer(
    model=model,
    args=training_args,
    compute_metrics=compute_metrics,
    data_collator=data_collator,
    train_dataset=train_dataset,
    eval_dataset=val_dataset,
    # prediction_loss_only=True,
)


trainer.train()

"""After training is done you can save the model by calling `save_model()`. This will save the trained model to our `output_dir` from our `TrainingArguments`."""

trainer.save_model()

from transformers import pipeline

# chef = pipeline('text-generation',model='./gpt2-last', tokenizer=pretrainedmodel,config={'max_length':3000})
genphrase = pipeline('text-generation',model='./gpt2-last', tokenizer=pretrainedmodel)



prod_data = load_dataset("csv", data_files=[
                             '../input/gpt2finnishdata/testcased.txt'], delimiter="\t", column_names=["ask"], script_version="master")


f = open("0seq2seqres.txt", "w")

for ask in prod_data['train']['ask']:

     res = genphrase(ask,max_length=250,min_length=50)[0]['generated_text']
     f.write(res+'\n\n')


con = sqlite3.connect("../input/gpt2finnishdata/singo.db")
df = pd.read_sql_query("SELECT phrase from phrases ORDER BY RANDOM() limit 50", con)

# Verify that result of SQL query is stored in the dataframe
# print(df.head())

con.close()

work_data = Dataset.from_pandas(df)

f.write('----------------------------\n\n')
for entity in work_data:
    phrase = entity['Phrase']
    phrase = phrase[0].upper()+phrase[1:]
    print(phrase)
    res = genphrase(phrase,max_length=250,min_length=50,num_return_sequences=5)[0]['generated_text']
    f.write(res+'\n\n')


f.close()    
