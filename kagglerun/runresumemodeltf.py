
import os
# os.system('pip install --upgrade tensorflow==2.2.0')
# os.system('pip install --upgrade transformers==4.4.0')
# os.system('pip install --upgrade tokenizers')
# os.system('pip install --upgrade transformers==4.1.1')
from transformers import WEIGHTS_NAME, CONFIG_NAME
from pathlib import Path

# os.system('pip install --upgrade wandb')
#os.system('pip install --upgrade tensorflow==2.3.0')
# os.system('pip install --upgrade transformers==4.4.0')

import tensorflow as tf
from transformers import GPT2Config, TFGPT2LMHeadModel, GPT2Tokenizer

from tokenizers.models import BPE
from tokenizers import Tokenizer
from tokenizers.decoders import ByteLevel as ByteLevelDecoder
from tokenizers.normalizers import NFKC, Sequence
from tokenizers.pre_tokenizers import ByteLevel
from tokenizers.trainers import BpeTrainer


import time
from keras.callbacks import Callback


class TimerCallback(Callback):
    
    def __init__(self, maxExecutionTime, byBatch = False, on_interrupt=None):
        
# Arguments:
#     maxExecutionTime (number): Time in minutes. The model will keep training 
#                                until shortly before this limit
#                                (If you need safety, provide a time with a certain tolerance)

#     byBatch (boolean)     : If True, will try to interrupt training at the end of each batch
#                             If False, will try to interrupt the model at the end of each epoch    
#                            (use `byBatch = True` only if each epoch is going to take hours)          

#     on_interrupt (method)          : called when training is interrupted
#         signature: func(model,elapsedTime), where...
#               model: the model being trained
#               elapsedTime: the time passed since the beginning until interruption   

        
        self.maxExecutionTime = maxExecutionTime * 60
        self.on_interrupt = on_interrupt
        
        #the same handler is used for checking each batch or each epoch
        if byBatch == True:
            #on_batch_end is called by keras every time a batch finishes
            self.on_batch_end = self.on_end_handler
        else:
            #on_epoch_end is called by keras every time an epoch finishes
            self.on_epoch_end = self.on_end_handler
    #Keras will call this when training begins
    def on_train_begin(self, logs):
        self.startTime = time.time()
        self.longestTime = 0            #time taken by the longest epoch or batch
        self.lastTime = self.startTime  #time when the last trained epoch or batch was finished
    
    
    #this is our custom handler that will be used in place of the keras methods:
        #`on_batch_end(batch,logs)` or `on_epoch_end(epoch,logs)`
    def on_end_handler(self, index, logs):
        
        currentTime      = time.time()                           
        self.elapsedTime = currentTime - self.startTime    #total time taken until now
        thisTime         = currentTime - self.lastTime     #time taken for the current epoch
                                                               #or batch to finish
        
        self.lastTime = currentTime
        
        #verifications will be made based on the longest epoch or batch
        if thisTime > self.longestTime:
            self.longestTime = thisTime
        
        
        #if the (assumed) time taken by the next epoch or batch is greater than the
            #remaining time, stop training
        remainingTime = self.maxExecutionTime - self.elapsedTime
        if remainingTime < self.longestTime:
            
            self.model.stop_training = True  #this tells Keras to not continue training
            print("\n\nTimerCallback: Finishing model training before it takes too much time. (Elapsed time: " + str(self.elapsedTime/60.) + " minutes )\n\n")
            
            #if we have passed the `on_interrupt` callback, call it here
            if self.on_interrupt is not None:
                self.on_interrupt(self.model, self.elapsedTime)    


class BPE_token(object):
    def __init__(self):
        self.tokenizer = Tokenizer(BPE())
        self.tokenizer.normalizer = Sequence([
            NFKC()
        ])
        self.tokenizer.pre_tokenizer = ByteLevel()
        self.tokenizer.decoder = ByteLevelDecoder()

    def bpe_train(self, paths):
        trainer = BpeTrainer(vocab_size=50000, show_progress=True, special_tokens=[
        # trainer = BpeTrainer(vocab_size=50000, show_progress=True, inital_alphabet=ByteLevel.alphabet(), special_tokens=[
            "<s>",
            "<pad>",
            "</s>",
            "<unk>",
            "<mask>"
        ])
        self.tokenizer.train(trainer, paths)

    def save_tokenizer(self, location, prefix=None):
        if not os.path.exists(location):
            os.makedirs(location)
        self.tokenizer.model.save(location, prefix)


import wandb
from wandb.keras import WandbCallback

os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

runnum = "11"

epochs = 50
if runnum == "":
  epochs = 1

acc = 'alesandermazurov'
# best = 0

output_dir = './figpt2model/'

# run = wandb.init(project="creategpt2model", config={"epochs": epochs, "trainfile": train_path, "eval_steps": eval_steps,
#                                                     "warmup_steps": warmup_steps, "best": best, 'batch_size': batch_size, 'runnum': runnum, 'acc': acc,'job':'resumemodel'})

run = wandb.init(project="createmodelgpt2finnish", entity="remotejob2", config={
                 'acc': acc, 'job': 'resumemodel', "epochs": epochs, 'runnum': runnum})

paths = [str(x)
         for x in Path("../input/gpt2finnishdata/").glob("**/train*.txt")]

if runnum == "":
  tokenizer = BPE_token()
  # train the tokenizer model
  tokenizer.bpe_train(paths)
  tokenizer.save_tokenizer(output_dir)
 
  tokenizer = GPT2Tokenizer.from_pretrained(output_dir)
  tokenizer.add_special_tokens({
    "eos_token": "</s>",
    "bos_token": "<s>",
    "unk_token": "<unk>",
    "pad_token": "<pad>",
    "mask_token": "<mask>"
})
  config = GPT2Config(
      vocab_size=tokenizer.vocab_size,
      bos_token_id=tokenizer.bos_token_id,
      eos_token_id=tokenizer.eos_token_id
  )
# creating the model
  model = TFGPT2LMHeadModel(config)

else:

  artifactdir = 'remotejob2/createmodelgpt2finnish/gpt2finnish_checkpointmodel:v'+runnum
  artifact = run.use_artifact(artifactdir, type='model')
  artifact_dir = artifact.download()
  print('artifact_dir', artifact_dir)

  tokenizer = GPT2Tokenizer.from_pretrained(artifact_dir)
  model = TFGPT2LMHeadModel.from_pretrained(artifact_dir)


single_string = ''
for filename in paths:
  with open(filename, "r", encoding='utf-8') as f:
    x = f.read()

  single_string += x + tokenizer.eos_token


string_tokenized = tokenizer.encode(single_string)

examples = []
block_size = 100
BATCH_SIZE = 12
BUFFER_SIZE = 1000
for i in range(0, len(string_tokenized) - block_size + 1, block_size):
  examples.append(string_tokenized[i:i + block_size])
inputs, labels = [], []
for ex in examples:
  inputs.append(ex[:-1])
  labels.append(ex[1:])
dataset = tf.data.Dataset.from_tensor_slices((inputs, labels))
dataset = dataset.shuffle(BUFFER_SIZE).batch(BATCH_SIZE, drop_remainder=True)

# defining our optimizer
optimizer = tf.keras.optimizers.Adam(
    learning_rate=3e-5, epsilon=1e-08, clipnorm=1.0)
# definining our loss function
loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
# defining our metric which we want to observe
metric = tf.keras.metrics.SparseCategoricalAccuracy('accuracy')

model.compile(optimizer=optimizer, loss=[
              loss, *[None] * model.config.n_layer], metrics=[metric])

# num_epoch = epochs

# history = model.fit(dataset, epochs=wandb.config.epochs,callbacks=[WandbCallback()])

timerCallback = TimerCallback(520,byBatch=True)

model.fit(dataset, epochs=wandb.config.epochs,callbacks=[WandbCallback(),timerCallback])

# creating directory if it is not present
if not os.path.exists(output_dir):
  os.mkdir(output_dir)
model_to_save = model.module if hasattr(model, 'module') else model
output_model_file = os.path.join(output_dir, WEIGHTS_NAME)
output_config_file = os.path.join(output_dir, CONFIG_NAME)
# save model and model configs
model.save_pretrained(output_dir)
model_to_save.config.to_json_file(output_config_file)
# save tokenizer
tokenizer.save_pretrained(output_dir)

artifact = wandb.Artifact('gpt2finnish_checkpointmodel', type='model')
for entry in os.scandir(path=output_dir):
    print("wd",output_dir+entry.name)
    artifact.add_file(output_dir+entry.name)

run.log_artifact(artifact)
