export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
cp kernelgpu/kernel-metadata.json.createmodeltf.supportsupport kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelgpu/kernel-metadata.json.createmodeltf.remotejob kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/