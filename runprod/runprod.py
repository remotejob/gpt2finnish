

import tensorflow as tf
from transformers import GPT2Config, TFGPT2LMHeadModel, GPT2Tokenizer, set_seed
import sqlite3
import requests


tokenizer = GPT2Tokenizer.from_pretrained(
    '../input/gpt2finnishdata/figpt2model')
model = TFGPT2LMHeadModel.from_pretrained(
    '../input/gpt2finnishdata/figpt2model')

try:
    cacheurl0 = 'file:../input/gpt2finnishdata/singo.db' + '?mode=rwc&cache=shared'
    sqliteConnection = sqlite3.connect(cacheurl0, uri=True)
    cursor = sqliteConnection.cursor()

    sqlite_select_Query = "select sqlite_version();"
    cursor.execute(sqlite_select_Query)
    record = cursor.fetchall()
    cursor.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)


n = 0

while True:

    prifixes = []

    cursor = sqliteConnection.execute(
        "SELECT phrase from phrases ORDER BY RANDOM() limit 10")
    for row in cursor:
        prifixes.append(str(row[0]))

    for ask in prifixes:

        set_seed(n)
        tf.random.set_seed(n)
        n = n + 1

        input_ids = tokenizer.encode(ask.capitalize(), return_tensors='tf')
        beam_output = model.generate(
            input_ids,
            max_length=512,
            # num_beams=3,
            # temperature=0.7,
            # top_p=0.92,
            top_k=50,
            top_p=0.95,
            # no_repeat_ngram_size=2,
            num_return_sequences=12,
            early_stopping=True,
            do_sample=True,
        )

        out = []

        for phrase in beam_output:

            orgtxt = tokenizer.decode(phrase, skip_special_tokens=True)

            pload = {'Orgtxt': orgtxt}
            out.append(pload)

        r = requests.post(
            'http://104.131.171.163:6000/v3/insertque', json=out)
        r.close()
