# export KAGGLE_CONFIG_DIR=/home/juno/kagglesupportsupport
# cp kernelgpu/kernel-metadata.json.createmodeltf.supportsupport kagglerun/kernel-metadata.json
# kaggle kernels push -p kagglerun/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelgpu/kernel-metadata.json.resumemodeltf.remotejob kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/


export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp kernelgpu/kernel-metadata.json.resumemodeltf.almazurov kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/


unset KAGGLE_CONFIG_DIR
cp kernelgpu/kernel-metadata.json.resumemodeltf.alesandermazurov kagglerun/kernel-metadata.json
kaggle kernels push -p kagglerun/
